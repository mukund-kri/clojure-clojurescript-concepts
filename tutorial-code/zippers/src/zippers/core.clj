(ns zippers.core
  (:require [clojure.zip :as zip])
  (:gen-class))

;;;; Zippers demo code.
;; Code I'v written when following this video
;; https://www.youtube.com/watch?v=GzM9ASu2luw

;; starting out by copying out the data structure given in the video.
(def data [1
           [2 3 4]
           [[5 6]
            [7 8]
            [[9 10 11]]]
           12])

;; A very basic operation, get the first node with zip/down
(-> (zip/vector-zip data)
    (zip/down)
    (zip/node))

;; A lot more movement over the data structure
(-> (zip/vector-zip data)
    (zip/down)
    (zip/right)
    (zip/right)
    (zip/down)
    (zip/node))

;; replace and edit to produce a new ds with changed values
(-> (zip/vector-zip data)
    (zip/down)
    (zip/right)
    (zip/down)
    (zip/replace 20)
    (zip/root)
    )

(-> (zip/vector-zip data)
    (zip/down)
    (zip/right)
    (zip/down)
    (zip/edit inc)
    (zip/root)
    )

;;;; The zip/next function. 
;; Supports a "breath-first" traversal of the given data structure
(-> (zip/vector-zip data)
    (zip/next)
    (zip/next)
    (zip/node))


;;;; Implement zip-map as shown in the video
(defn zip-map [f tree]
  (loop [node tree]
    (if (identical? (zip/next node) node)
      (zip/root node)
      (if (zip/branch? node)
        (recur (zip/next node))
        (recur (-> node (zip/edit f) zip/next))))))




(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
