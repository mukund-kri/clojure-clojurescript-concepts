(ns iteration.core
  (:use [iteration.loop-examples :as loop-examples])
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (loop-examples/run))
