(ns iteration.loop-examples
  (:gen-class))


(defn count-down-to-zero [n]
  "A very simple demo of the loop form. Count down till zero from n"
  (loop [n n]
    (when (> n 0)
      (println n)
      (recur (dec n)))))

;; Now for something more fuctional
(defn fibonacci [n]
  "Compute the fibonacci series as a vector of ints up-to n"
  (loop [a1 1
         a2 1
         acc [1 1]]
    (let [sum (+ a1 a2)]
      (if (>= sum n)
        acc
        (recur a2 sum (conj acc sum))))))

(defn run []
  "Run the examples given in this file"
  (println "Welcome to the loop construct")
  )
