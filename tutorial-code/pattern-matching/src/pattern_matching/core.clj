(ns pattern-matching.core
  (:require [clojure.core.match :refer [match]])
  (:gen-class))


(defn is-text? [node]
  (->> node
       first
       type
       (= java.lang.String)))

;;;; REST PATTERN + GUARD
(defn tag-type [tag]
  (match [tag]
         [([:h1 & r] :seq)] "Head Tag"
         [(x :guard is-text?)] "Html text"
         :else "No Match"))


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
