(ns funtions.recursion)

(defn fibonacci-inner [a1 a2 n] 
  (let [sum (+ a1 a2)]
    (if (> sum n)
      []
      (cons sum (fibonacci-inner a2 sum n)))))

(defn fibonacci [n]
  (->> (fibonacci-inner 1 1 n)
      (cons 1)
      (cons 1)))
